﻿using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace laba2
{
    class Crypto
    {
        //---Constant programm---
        const string EncrFolder = @"Encrypt\";
        const string DecrFolder = @"Decrypt\";
        const string SrcFolder = @"docs\";
        //---File works---
        static public string FileWork(string FileName){
            if (File.Exists(EncrFolder + FileName)){
                Console.WriteLine("File enable.");
                Console.WriteLine("ReWrite? (y/n)");
                string ch = Console.ReadLine();
                if (ch == "y"){
                    File.Delete(EncrFolder + FileName);
                    Console.WriteLine("Get INFO:");
                    string text = Console.ReadLine();
                    using (StreamWriter sw = new StreamWriter(EncrFolder + FileName, false, System.Text.Encoding.Default))
                    {
                        sw.WriteLine(text);
                    }
                    return text;
                }
                else {
                    using (StreamReader sr = new StreamReader(EncrFolder + FileName))
                    {
                        string text = sr.ReadToEnd();
                        return text;
                    }
                }

            }
            else {
                Console.WriteLine("File disable.");
                Console.WriteLine("Get INFO:");
                string text = Console.ReadLine();
                using (StreamWriter sw = new StreamWriter(EncrFolder + FileName, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(text);
                }
                return text;
            }

        }  
        //---Algoritm Menu---
        static public void Menu()
        {
            Console.Clear();
            //Выводим меню, его пункты с соответствующими цифрами\символами
            Console.WriteLine("### MENU Crypto###");
            Console.WriteLine("1. RijndaleManaged");
            Console.WriteLine("2. RC2");
            Console.WriteLine("3. RSA");
            Console.WriteLine("4. DiffiHelman");
            Console.WriteLine("5. Exit");

            Console.Write("\n" + "Введите команду: ");
 
            char ch = char.Parse(Console.ReadLine()); //Тут желательно сделать проверку, или считывать всю строку, и в switch уже отсеивать
 
            switch (ch)
            {
                case '1': Menu2("RijndaleManaged"); break;
                case '2': Menu2("RC2"); break;
                case '3': Menu2("RSA"); break;
                case '4': Menu2("DiffiHelman"); break;
                case '5': break;
 
            }    
        }
        //---Crypt/Decrypt Menu---
        static public void Menu2(string algoritm)
        {
            Console.Clear();
            //Выводим меню, его пункты с соответствующими цифрами\символами
            Console.WriteLine("### MENU Crypto###");
            Console.WriteLine("1. Crypt");
            Console.WriteLine("2. Decrypt");
            Console.WriteLine("3. Exit");

            Console.Write("\n" + "Введите команду: ");
 
            char ch = char.Parse(Console.ReadLine()); //Тут желательно сделать проверку, или считывать всю строку, и в switch уже отсеивать
 
            switch (ch)
            {
                case '1': 
                if (algoritm == "RijndaleManaged"){
                    Console.Write("\n" + "Filename: ");
                    string FileName = (Console.ReadLine()) + ".txt";
                    string text = FileWork(FileName);
                    EncryptFile(FileName);
                    

                }
                else if (algoritm == "RC2"){
                    Console.Write("\n" + "Filename: ");
                    string FileName = (Console.ReadLine()) + ".txt";
                    string text = FileWork(FileName);
                    EncryptFileRC2(FileName);

                }
                else if (algoritm == "RSA"){
                    Console.Write("\n" + "Filename: ");
                    string FileName = (Console.ReadLine()) + ".txt";
                    string text = FileWork(FileName);

                }
                else if (algoritm == "DiffiHelman"){
                    Console.Write("\n" + "Filename: ");
                    string FileName = (Console.ReadLine()) + ".txt";
                    string text = FileWork(FileName);

                }
                else {

                }
                break;
                case '2': 
                if (algoritm == "RijndaleManaged"){
                    Console.Write("\n" + "Filename: ");
                    string FileName = (Console.ReadLine()) + ".enc";
                    DecryptFile(FileName);

                }
                else if (algoritm == "RC2"){
                    Console.Write("\n" + "Filename: ");
                    string FileName = (Console.ReadLine()) + ".enc";
                    DecryptFileRC2(FileName);

                }
                else if (algoritm == "RSA"){
                    Console.Write("\n" + "Filename: ");
                    string FileName = (Console.ReadLine()) + ".txt";
                    string text = FileWork(FileName);

                }
                else if (algoritm == "DiffiHelman"){
                    Console.Write("\n" + "Filename: ");
                    string FileName = (Console.ReadLine()) + ".txt";
                    string text = FileWork(FileName);

                }
                else {

                }
                break;
                case '3': Menu(); break;
 
            }    
        }
        //---Main---
        static void Main(string[] args)
        {
            Menu();
        }
        //---DiffiHelman---

        //---RSA Algoritms---

        //---RC2 Algoritms---
        static private void EncryptFileRC2(string inFile)
        {

            RC2 RC2alg = RC2.Create();
            string Data = null;
            byte[] keyR = RC2alg.Key; 
            byte[] IVR = RC2alg.IV;

            byte[] keyEncrypted = keyR;

            byte[] LenK = new byte[4];
            byte[] LenIV = new byte[4];

            int lKey = keyEncrypted.Length;
            LenK = BitConverter.GetBytes(lKey);
            int lIV = IVR.Length;
            LenIV = BitConverter.GetBytes(lIV);


            int startFileName = inFile.LastIndexOf("\\") + 1;
            string outFile = EncrFolder + inFile.Substring(startFileName, inFile.LastIndexOf(".") - startFileName) + ".enc";

            using (FileStream outFs = new FileStream(outFile, FileMode.Create))
            {
                using (FileStream inFs = new FileStream(EncrFolder + inFile, FileMode.Open))
                    {
                        byte[] array = new byte[inFs.Length];
                // считываем данные
                        inFs.Read(array, 0, array.Length);
                // декодируем байты в строку
                        string textFromFile = System.Text.Encoding.Default.GetString(array);
                        Data = textFromFile;
                        inFs.Close();
                    }

                outFs.Write(LenK, 0, 4);
                outFs.Write(LenIV, 0, 4);
                outFs.Write(keyEncrypted, 0, lKey);
                outFs.Write(IVR, 0, lIV);

                CryptoStream cStream = new CryptoStream(outFs, RC2alg.CreateEncryptor(keyR,IVR), CryptoStreamMode.Write); 

            // Create a StreamWriter using the CryptoStream.
                StreamWriter sWriter = new StreamWriter(cStream);

            // Write the data to the stream 
            // to encrypt it.
                sWriter.WriteLine(Data);
  
            // Close the streams and
            // close the file.
                sWriter.Close();
                cStream.Close();
                outFs.Close();
            }
        }
        static private void DecryptFileRC2(string inFile)
        {

            RC2 RC2alg = RC2.Create();

            byte[] LenK = new byte[4];
            byte[] LenIV = new byte[4];

            string outFile = DecrFolder + inFile.Substring(0, inFile.LastIndexOf(".")) + ".txt";

            using (FileStream inFs = new FileStream(EncrFolder + inFile, FileMode.Open))
            {

                inFs.Seek(0, SeekOrigin.Begin);
                inFs.Seek(0, SeekOrigin.Begin);
                inFs.Read(LenK, 0, 3);
                inFs.Seek(4, SeekOrigin.Begin);
                inFs.Read(LenIV, 0, 3);

                int lenK = BitConverter.ToInt32(LenK, 0);
                int lenIV = BitConverter.ToInt32(LenIV, 0);

                int startC = lenK + lenIV + 8;
                int lenC = (int)inFs.Length - startC;

                byte[] KeyEncrypted = new byte[lenK];
                byte[] IV = new byte[lenIV];

                inFs.Seek(8, SeekOrigin.Begin);
                inFs.Read(KeyEncrypted, 0, lenK);
                inFs.Seek(8 + lenK, SeekOrigin.Begin);
                inFs.Read(IV, 0, lenIV);
                Directory.CreateDirectory(DecrFolder);



                using (FileStream outFs = new FileStream(outFile, FileMode.Create))
                {

                    int count = 0;
                    int offset = 0;

                    int blockSizeBytes = RC2alg.BlockSize / 8;
                    byte[] data = new byte[blockSizeBytes];

                    inFs.Seek(startC, SeekOrigin.Begin);

                    
                    using (CryptoStream outStreamDecrypted = new CryptoStream(outFs, RC2alg.CreateDecryptor(KeyEncrypted,IV), CryptoStreamMode.Write))
                    {
                        do
                        {
                            count = inFs.Read(data, 0, blockSizeBytes);
                            offset += count;
                            outStreamDecrypted.Write(data, 0, count);
                        }
                        while (count > 0);

                        outStreamDecrypted.FlushFinalBlock();
                        outStreamDecrypted.Close();
                    }
                    outFs.Close();
                }
                inFs.Close();
            }
        }
        //---Reindale algotitms ---
        static private void EncryptFile(string inFile)
        {

            RijndaelManaged rjndl = new RijndaelManaged();

            rjndl.GenerateKey();
            rjndl.GenerateIV();
            byte[] keyR = rjndl.Key; 
            byte[] IVR = rjndl.IV;

            byte[] keyEncrypted = keyR;

            byte[] LenK = new byte[4];
            byte[] LenIV = new byte[4];

            int lKey = keyEncrypted.Length;
            LenK = BitConverter.GetBytes(lKey);
            int lIV = IVR.Length;
            LenIV = BitConverter.GetBytes(lIV);

            ICryptoTransform transform = rjndl.CreateEncryptor(keyR, IVR);

            int startFileName = inFile.LastIndexOf("\\") + 1;
            string outFile = EncrFolder + inFile.Substring(startFileName, inFile.LastIndexOf(".") - startFileName) + ".enc";

            using (FileStream outFs = new FileStream(outFile, FileMode.Create))
            {

                outFs.Write(LenK, 0, 4);
                outFs.Write(LenIV, 0, 4);
                outFs.Write(keyEncrypted, 0, lKey);
                outFs.Write(IVR, 0, lIV);

                using (CryptoStream outStreamEncrypted = new CryptoStream(outFs, transform, CryptoStreamMode.Write))
                {

                    int count = 0;
                    int offset = 0;

                    int blockSizeBytes = rjndl.BlockSize / 8;
                    byte[] data = new byte[blockSizeBytes];
                    int bytesRead = 0;

                    using (FileStream inFs = new FileStream(EncrFolder + inFile, FileMode.Open))
                    {
                        do
                        {
                            count = inFs.Read(data, 0, blockSizeBytes);
                            offset += count;
                            outStreamEncrypted.Write(data, 0, count);
                            bytesRead += blockSizeBytes;
                        }
                        while (count > 0);
                        inFs.Close();
                    }
                    outStreamEncrypted.FlushFinalBlock();
                    outStreamEncrypted.Close();
                }
                outFs.Close();
            }
        }
        static private void DecryptFile(string inFile)
        {

            RijndaelManaged rjndl = new RijndaelManaged();

            byte[] LenK = new byte[4];
            byte[] LenIV = new byte[4];

            string outFile = DecrFolder + inFile.Substring(0, inFile.LastIndexOf(".")) + ".txt";

            using (FileStream inFs = new FileStream(EncrFolder + inFile, FileMode.Open))
            {

                inFs.Seek(0, SeekOrigin.Begin);
                inFs.Seek(0, SeekOrigin.Begin);
                inFs.Read(LenK, 0, 3);
                inFs.Seek(4, SeekOrigin.Begin);
                inFs.Read(LenIV, 0, 3);

                int lenK = BitConverter.ToInt32(LenK, 0);
                int lenIV = BitConverter.ToInt32(LenIV, 0);

                int startC = lenK + lenIV + 8;
                int lenC = (int)inFs.Length - startC;

                byte[] KeyEncrypted = new byte[lenK];
                byte[] IV = new byte[lenIV];

                inFs.Seek(8, SeekOrigin.Begin);
                inFs.Read(KeyEncrypted, 0, lenK);
                inFs.Seek(8 + lenK, SeekOrigin.Begin);
                inFs.Read(IV, 0, lenIV);
                Directory.CreateDirectory(DecrFolder);

                byte[] KeyDecrypted = KeyEncrypted;

                ICryptoTransform transform = rjndl.CreateDecryptor(KeyDecrypted, IV);

                using (FileStream outFs = new FileStream(outFile, FileMode.Create))
                {

                    int count = 0;
                    int offset = 0;

                    int blockSizeBytes = rjndl.BlockSize / 8;
                    byte[] data = new byte[blockSizeBytes];

                    inFs.Seek(startC, SeekOrigin.Begin);
                    using (CryptoStream outStreamDecrypted = new CryptoStream(outFs, transform, CryptoStreamMode.Write))
                    {
                        do
                        {
                            count = inFs.Read(data, 0, blockSizeBytes);
                            offset += count;
                            outStreamDecrypted.Write(data, 0, count);
                        }
                        while (count > 0);

                        outStreamDecrypted.FlushFinalBlock();
                        outStreamDecrypted.Close();
                    }
                    outFs.Close();
                }
                inFs.Close();
            }
        }
    }
}
